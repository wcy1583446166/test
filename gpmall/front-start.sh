#!/bin/bash
nohup java -jar /root/user-provider-0.0.1-SNAPSHOT.jar &
sleep 6
nohup java -jar /root/shopping-provider-0.0.1-SNAPSHOT.jar &
sleep 6
nohup java -jar /root/gpmall-shopping-0.0.1-SNAPSHOT.jar &
sleep 6
nohup java -jar /root/gpmall-user-0.0.1-SNAPSHOT.jar &
sleep 6
nginx -g "daemon off;"
